CREATE TABLE images(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    url TEXT(255) DEFAULT '' NOT NULL,
    nom TEXT(100) DEFAULT '',
    extension TEXT(10) DEFAULT '',
    alt TEXT(255) DEFAULT '',
    titre TEXT(100) DEFAULT '',
    largeur TEXT(50) DEFAULT '',
    hauteur TEXT(50) DEFAULT '',
    id_evenement INTEGER,
    FOREIGN KEY(id_evenement) REFERENCES evenements_culturels(id_evenement));
CREATE TABLE videos(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    src TEXT(255) DEFAULT '' NOT NULL,
    largeur TEXT(50) DEFAULT '',
    hauteur TEXT(50) DEFAULT '',
    id_evenement INTEGER,
    FOREIGN KEY(id_evenement) REFERENCES evenements_culturels(id_evenement));
CREATE TABLE evenements_culturels(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    titre TEXT DEFAULT '' NOT NULL,
    date TEXT DEFAULT '',
    tarif TEXT DEFAULT '',
    texte TEXT DEFAULT '',
    id_evenement INTEGER NOT NULL);
