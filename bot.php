<?php
    
    include('pdo.php');

    require __DIR__ . "/vendor/autoload.php";

    use GuzzleHttp\Client;
    use Symfony\Component\DomCrawler\Crawler;

    /////////////////////////FONCTIONS////////////////////////////////////////////////////////////

    //Fonction qui renvoie le corps d'une page web
    function getBody($adress,$path) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $adress
        ]);
        $response = $client->request('GET', $path);
        $status = $response->getStatusCode();
        if ($status !== 200){
            return;
        }
        

        return (string) $response->getBody();
    }
    
    //Fonction qui renvoie un tableau des href des balises a d'une page web
    function getAHref($adress,$path) {

        $html = getBody($adress,$path);
        $crawler = new Crawler($html);

        $crawler = $crawler->filter('a');
        $arrayHref = array();
        $i = 0;
        foreach ($crawler as $domElement) {
            if($domElement->getAttribute('href') !== ""){
                
                $arrayHref[$i] = $domElement->getAttribute('href');
                $i++;
            }
        }
        return $arrayHref;
    }

    //Fonction qui renvoie un tableau des "infos" des balises img d'une page web
    function getImgInfo($adress,$path,$info,$tag1) {

        $html = getBody($adress,$path);
        $crawler = new Crawler($html);

        $crawler = $crawler->filter($tag1.' img');
        $array = array();
        $i = 0;
        foreach ($crawler as $domElement) {
            if($domElement->getAttribute('src') !== ""){
                
                $array[$i] = $domElement->getAttribute($info);
                $i++;
            }
        }
        return $array;
    }

    //Fonction qui sauvegarde dans la bdd les données récupérées sur les images
    function saveImgInfo($adress,$href,$bdd,$tag1,$id,$globalAdress = "") {

        //récupère un tableau de toutes les sources des balises d'images d'une page
        $arraySrc = getImgInfo($adress,$href,"src",$tag1);
        //récupère un tableau de tout les "alt" des balises images d'une page
        $arrayAlt = getImgInfo($adress,$href,"alt",$tag1);
        //récupère un tableau de tout les titles des balises images d'une page
        $arrayTitle = getImgInfo($adress,$href,"title",$tag1);
        //récupère un tableau de tout les width des balises images d'une page
        $arrayWidth = getImgInfo($adress,$href,"width",$tag1);
        //récupère un tableau de tout les heigth des balises images d'une page
        $arrayHeight = getImgInfo($adress,$href,"heigth",$tag1);
        $i = 0;

        //Pour chaque source d'image dans le tableau des sources d'images
        foreach ($arraySrc as $src) {

            // Cherche et récupère le nom et l'extension de la source
            $arrayToSort = explode("/",$src);
            $length = count($arrayToSort);
            $arrayToSort = explode(".",$arrayToSort[$length -1]);
            $length = count($arrayToSort);
            $name = $arrayToSort[0];
            $type = $arrayToSort[$length-1];

            

            //vérifie si une source d'image est déjà dans la bdd
            $alreadyIn = false;
            $reponse = $bdd->query('SELECT url FROM images');
            while ($donnees = $reponse->fetch())
            {
                if($donnees['url'] === $globalAdress.$src){
                    $alreadyIn = true;
                }
            }
            //Si la source n'est pas déjà dans la bdd alors insère les données d'une image dans la bdd
            if($alreadyIn === false) {
                $req = $bdd->prepare('INSERT INTO images(url,nom,extension,alt,titre,largeur,hauteur,id_evenement) 
                VALUES(:url, :nom, :extension, :alt, :titre, :largeur, :hauteur, :id_evenement)');
                $req->execute(array(
                    'url' => $globalAdress.$src,
                    'nom' => $name,
                    'extension' => $type,
                    'alt' => $arrayAlt[$i],
                    'titre' => $arrayTitle[$i],
                    'largeur' => $arrayWidth[$i],
                    'hauteur' => $arrayHeight[$i],
                    'id_evenement' => $id
                ));
            }
            $i++;
        }
    }

    //Fonction qui renvoie un tableau des "infos" des balises iframe d'une page web
    function getVideoInfo($adress,$path,$info) {

        $html = getBody($adress,$path);
        $crawler = new Crawler($html);

        $crawler = $crawler->filter("iframe");
        $array = array();
        $i = 0;
        foreach ($crawler as $domElement) {
            if($domElement->getAttribute('src') !== ""){
                
                $array[$i] = $domElement->getAttribute($info);
                $i++;
            }
        }
        return $array;
    }

    //Fonction qui sauvegarde dans la bdd les données récupérées sur les videos
    function saveVideoInfo($adress,$href,$bdd,$id) {

        //récupère un tableau de toutes les sources des balises iframes d'une page
        $arrayVideoSrc = getVideoInfo ($adress,$href,"src");
        //récupère un tableau de tout les width des balises iframes d'une page
        $arrayVideoWidth = getVideoInfo ($adress,$href,"width");
        //récupère un tableau de tout les height des balises iframes d'une page
        $arrayVideoHeight = getVideoInfo ($adress,$href,"height");
        $j = 0;

        //Pour chaque source de vidéos dans le tableau des sources de vidéos
        foreach ($arrayVideoSrc as $videoSrc) {

            //vérifie si une source d'image est déjà dans la bdd
            $alreadyIn = false;
            $reponse = $bdd->query('SELECT src FROM videos');
            while ($donnees = $reponse->fetch())
            {
                if($donnees['src'] === $videoSrc){
                    $alreadyIn = true;
                }
            }
            //Si la source n'est pas déjà dans la bdd alors insère les données d'une image dans la bdd
            if($alreadyIn === false) {
                $req = $bdd->prepare('INSERT INTO videos(src,largeur,hauteur,id_evenement) 
                VALUES(:src, :largeur, :hauteur, :id_evenement)');
                $req->execute(array(
                    'src' => $videoSrc,
                    'largeur' => $arrayVideoWidth[$j],
                    'hauteur' => $arrayVideoHeight[$j],
                    'id_evenement' => $id
                ));
            }
            $j++;
        }
    }
    //Fonction qui renvoie le contenu textuel d'une balise 
    function getTextTitle($adress,$path,$tag1,$tag2) {

        $html = getBody($adress,$path);
        $crawler = new Crawler($html);
        $textTitle = $crawler->filter($tag1." ".$tag2)->text();

        return $textTitle;
    }
    //Fonction qui renvoie le contenu textuel d'une balise 
    function getTextArticle($adress,$path,$tag1) {

        $html = getBody($adress,$path);
        $crawler = new Crawler($html);

        try {
            $textArticles = $crawler->filter($tag1." p")->extract(['_text']);
        } catch (Exception $e) {
            echo 'Exception reçue : ', $e->getMessage(), "\n";
            return;
        }
        return $textArticles;
    }

    //Fonction qui sauvegarde dans la bdd les données récupérées sur le texte
    function saveTextInfo($adress,$href,$bdd,$tag1,$tag2,$id) {

        $textTitle = getTextTitle($adress,$href,$tag1,$tag2);
        if($textTitle !== "") {
            $textArticles = getTextArticle($adress,$href,$tag1);

            $textArticle = '';
            foreach($textArticles as $text) {
                $textArticle .= $text.".";
            }
                
            //vérifie si un évènement est déjà dans la bdd
            $alreadyIn = false;
            $reponse = $bdd->query('SELECT titre FROM evenements_culturels');
            while ($donnees = $reponse->fetch())
            {
                if($donnees['titre'] === $textTitle){
                    $alreadyIn = true;
                }
            }
            if($alreadyIn === false) {
                $req = $bdd->prepare('INSERT INTO evenements_culturels(titre,texte,id_evenement) 
                VALUES(:titre,:texte,:id_evenement)');
                $req->execute(array(
                    'titre' => $textTitle,
                    'texte' => $textArticle,
                    'id_evenement' => $id
                ));
            }
        }
        
    }

    //Fonction qui parcourt des pages d'un site web et récupère des infos dans une bdd
    function getInfo($adress,$pages,$bdd,$tag1,$tag2,&$id,$globalAdress = "") {
        //récupère un tableau de tout les liens d'une page
        $arrayHref = getAHref($adress,"");

        
        //pour chaque lien dans le tableau de liens
        foreach ($arrayHref as $href) {

            //si le contenu de $pages est trouvé dans le nom d'un lien 
            if (strstr($href,$pages)!== false){
                
                //sauvegarde dans la bdd les infos des images de la page
                saveImgInfo($adress,$href,$bdd,$tag1,$id,$globalAdress);
                //sauvegarde dans la bdd les infos des vidéos de la page
                saveVideoInfo($adress,$href,$bdd,$id);
                //sauvegarde dans la bdd les infos textuel de la page
                saveTextInfo($adress,$href,$bdd,$tag1,$tag2,$id);
                $id++;
            }
            
        }
    }
    
    //////////////////////////////////MAIN////////////////////////////////////////////////
    // adresses de site web
    $adress1 = "https://www.mjc-chambery.com/liste-7-actualites.htm";
    $adress2 = "https://www.espacemalraux-chambery.fr/spectacles/#all";

    $id = 1;

    getInfo($adress1,"article",$bdd,"div[class='jumbotron']","h4",$id,"https://www.mjc-chambery.com/");
    getInfo($adress2,"spectacle",$bdd,"article","h1",$id);
?>

